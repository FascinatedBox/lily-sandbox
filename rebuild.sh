set -o errexit
cd "$(dirname "$(realpath "$0")")"

if [ ! -d "lily" ]; then
    echo "Cannot find lily in the sandbox."
    echo ""
    echo "Use Enter to clone from gitlab, or ctrl-c to stop."
    read
    git clone https://gitlab.com/FascinatedBox/lily/
fi

cd lily
git reset -q --hard HEAD
git pull -q
SRC_FILES=`ls src/*.c`
MINIMUM=`grep "cmake_minimum_required" CMakeLists.txt`
cd ..

lily -t build_cmakelists.lily "${MINIMUM}" "${SRC_FILES}" > lily/CMakeLists.txt
cp assets/sandbox.c lily/sandbox.c
lily filter_package_file.lily lily/src/lily_pkg_fs.c
lily filter_package_file.lily lily/src/lily_pkg_subprocess.c

cd lily
rm -f CMakeCache.txt
cmake .
make
mv lily_sandbox.js ../
